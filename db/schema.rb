# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130219042830) do

  create_table "houses", :force => true do |t|
    t.integer  "phase",                                     :null => false
    t.string   "plot_number",                               :null => false
    t.integer  "plot_id",                                   :null => false
    t.string   "number",                                    :null => false
    t.string   "status",      :default => "owner_occupied", :null => false
    t.integer  "rwh",         :default => 0
    t.integer  "area"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "members", :force => true do |t|
    t.integer  "member_id"
    t.string   "type"
    t.integer  "user_id"
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.integer  "plot_id"
    t.integer  "phase"
    t.string   "plot_number"
    t.string   "plot_status"
    t.integer  "plot_area"
    t.integer  "house_id"
    t.string   "house_number"
    t.string   "house_status"
    t.integer  "rwh"
    t.integer  "house_area"
    t.integer  "family_id"
    t.string   "family_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "plots", :force => true do |t|
    t.integer  "phase",                                    :null => false
    t.string   "number",                                   :null => false
    t.string   "status",     :default => "owner_occupied"
    t.integer  "area",       :default => 0
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name",            :null => false
    t.string   "email",           :null => false
    t.string   "password_digest"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

end
