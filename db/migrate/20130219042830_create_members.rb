class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.integer :member_id
      t.string :type

      t.integer :user_id
      t.string  :name
      t.string  :email
      t.string :password_digest

      t.integer :plot_id
      t.integer :phase
      t.string :plot_number
      t.string :plot_status
      t.integer :plot_area

      t.integer :house_id
      t.string  :house_number
      t.string  :house_status
      t.integer :rwh
      t.integer :house_area

      t.integer :family_id
      t.string :family_type
      t.timestamps
    end
  end
end
