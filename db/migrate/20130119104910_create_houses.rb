class CreateHouses < ActiveRecord::Migration
  def change
    create_table :houses do |t|
      t.integer :phase, :null => false
      t.string :plot_number, :null => false
      t.integer :plot_id, :null => false
      t.string  :number,  :null => false
      t.string  :status, :null => false, :default => "owner_occupied"
      t.integer :rwh, :default => 0
      t.integer :area

      t.timestamps
    end
  end
end
