class CreatePlots < ActiveRecord::Migration
  def change
    create_table :plots do |t|
      t.integer :phase, :null => false
      t.string :number, :null => false
      t.string :status, :default => 'owner_occupied'
      t.integer :area, :default => 0

      t.timestamps
    end
  end
end
