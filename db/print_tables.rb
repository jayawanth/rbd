require_relative '../app/controllers/helpers/members_helper'

class PrintTables
  include MembersHelper

  def print_plots
    plots = Plot.all
    puts "\n #{plots.count} Plots ..."
    s = "\t\t| %-1s-%-6s | %-15s | %-20s | %-30s | %-20s|\n"
    printf s, "Ph-", "Plot #", "Status", "Owner", "Owner Email", "Owner type"
    plots.each do |plot|
      print_plot(plot)
    end
  end

  def print_plot(plot)
    owner = plot.owner
    s = "\t\t| %-1s-%-6s | %-15s | %-20s | %-30s | %-20s|\n"
    printf s, plot.phase, plot.number, plot.status, owner.name, owner.email, owner.type
  end

  def print_houses
    houses = House.all
    puts "\n #{houses.count} Houses ..."
    s = "\t\t| %-1s-%-6s%-6s | %-15s | %-20s | %-30s | %-20s|\n"
    printf s, "Ph", "Plot #", "House #", "House Status", "Owner", "Owner Email", "Owner type"
    houses.each do |house|
      print_house(house)
    end
  end

  def print_house(house)
    owner = house.owner
    s = "\t\t| %-1s-%-6s%-6s | %-15s | %-20s | %-30s | %-20s|\n"
    printf s, house.phase, house.plot_number, house.number, house.status, owner.name, owner.email, owner.type
  end

  def print_users
    users = User.all
    puts "\n #{users.count} Users ..."
    users.each do |user|
      print_user(user)
    end
  end

  def print_user(user)
    s = "\t\t| %-20s | %-30s |\n"
    printf s, user.name, user.email
  end

  def print_plot_owners
    plot_owners = PlotOwner.all
    puts "\n #{plot_owners.count} Plot Owners ..."
    s = "\t\t| %-2s-%-6s| %-16s| %-5s | %-20s | %-31s|\n"
    printf s, 'Ph', 'Plot #', 'Plot Status', 'Area', 'Name', 'Email'

    plot_owners.each do |owner|
    s = "\t\t| %-1s-%-6s | %-15s | %-5s | %-20s | %-30s |\n"
      printf s, owner.phase, owner.plot_number, owner.plot_status, owner.plot_area, owner.name, owner.email
    end
  end

  def print_house_owners
    house_owners = HouseOwner.all
    puts "\n #{house_owners.count} House Owners ..."
    s = "\t\t| %-2s-%-6s | %-15s | %-5s | %-6s | %-15s | %-10s | %-6s | %-20s | %-30s |\n"
    printf s, 'Ph', 'Plot #', 'Plot Status', 'Area', 'House #', 'House Status',  'House Area', 'RWH', 'Name', 'Email'

    house_owners.each do |owner|
    s = "\t\t| %-1s-%-7s | %-15s | %-5s | %-7s | %-15s | %-10s | %-6s | %-20s | %-30s |\n"
      printf s, owner.phase, owner.plot_number, owner.plot_status, owner.plot_area, owner.house_number, owner.house_status, owner.house_area, owner.rwh, owner.name, owner.email
    end
  end

  def print_tenants
    tenants = Tenant.all
    puts "\n #{tenants.count} Tenants ..."
    s = "\t\t| %-2s-%-6s | %-7s | %-20s | %-30s | %-6s |\n"
    printf s, 'Ph', 'Plot #', 'House #', 'Name', 'Email', 'RWH'

    tenants.each do |tenant|
      s = "\t\t| %-2s-%-6s | %-7s | %-20s | %-30s | %-6s |\n"
      printf s, tenant.phase, tenant.plot_number, tenant.house_number, tenant.name, tenant.email, tenant.rwh
    end
  end

  def print_relatives
    relatives = Relative.all
    puts "\n #{relatives.count} Relatives ..."
    print_members(relatives)
  end

  def print_members(members)
    s = "\t\t| %-2s-%-6s | %-15s | %-5s | %-6s | %-15s | %-10s | %-6s | %-20s | %-30s | %-40s |\n"
    printf s, 'Ph', 'Plot #', 'Plot Status', 'Area', 'House #', 'House Status',  'House Area', 'RWH', 'Name', 'Email', 'Family of'

    members.each do |member|
      s = "\t\t| %-1s-%-7s | %-15s | %-5s | %-7s | %-15s | %-10s | %-6s | %-20s | %-30s | Family of %-30s |\n"
      printf s, member.phase, member.plot_number, member.plot_status, member.plot_area, member.house_number, member.house_status, member.house_area, member.rwh, member.name, member.email, member.family.email
    end
  end
end

