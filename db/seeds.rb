# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require_relative '../app/controllers/helpers/members_helper'

class RbdSeed
  include MembersHelper

  @@plots_houses = [
    { plot:
        { phase: 1, number: '5', status: 'house', area: 3544},
      house:
        { number: '5',      status: 'owner_occupied'}
    },
    { plot:
        { phase: 1, number: '6', status: 'empty', area: 3750},
    },
    { plot:
        { phase: 1, number: '15', status: 'empty', area: 3750},
    },
    { plot:
        { phase: 1, number: '16', status: 'house', area: 3000},
      house:
        {number: '16',     status: 'rented'},
    },
    { plot:
        { phase: 1, number: '48/1', status: 'house', area: 2400 },
      house:
        {number: '48/1A', status: 'owner_occupied'}
    },
    { plot:
        { phase: 1, number: '48/1', status: 'house', area: 2400 },
      house:
        {number: '48/1B', status: 'owner_occupied'}
    },
    { plot:
        { phase: 1, number: '123', status: 'house', area: 2400},
      house:
        {number: '123A',   status: 'rented'},
    },
    { plot:
        {phase: 1, number: '123', status: 'house', area: 2400},
      house:
        {number: '123B',   status: 'owner_occupied'},
    },
    { plot:
        { phase: 3, number: '249', status: 'house', area: 5366},
      house:
        {number: '249',    status: 'owner_occupied'},
    },
    { plot:
        { phase: 3, number: '250', status: 'empty', area: 2000},
    },
    { plot:
        { phase: 3, number: '259', status: 'house', area: 2509},
      house:
        {number: '259',    status: 'rented'},
    },
    { plot:
        { phase: 3, number: '333', status: 'house', area: 2055},
      house:
        {number: '333',    status: 'rented'},
    },
    { plot:
        { phase: 4, number: '372', status: 'construction', area: 4000},
    }
  ]

  def create_plots_and_houses
    Plot.delete_all
    House.delete_all
    puts "\nCreating Plots and Houses ..."
    @@plots_houses.each_with_index do |ph, i|
      plot, house = ph[:plot], ph[:house]
      @plot, @house = nil, nil
      puts "[#{i}:]"

      # valid inputs?
      if !plot
        puts " ERROR : : No plot specified #{ph}"
        next
      elsif house && plot[:status] != 'house' || !house && plot[:status] == 'house'
        puts " ERROR : Invalid plot[:status]: #{ph}"
        next
      end
      @plot = Plot.find_by_number(plot[:number])
      if @plot
        puts "INFO  : Plot '#{@plot.number}' already exists"
      else
        @plot = Plot.create!(plot)
        puts "\t: Plot #{@plot.number} Created"
      end

      if house
        house[:phase], house[:plot_number], house[:plot_id], house[:rwh], house[:area] = @plot.phase, @plot.number, @plot.id, 0, @plot.area/2
        @house = House.create!(house)
        puts "\t: House #{@house.number} Created"
      end
    end
    puts "Created #{Plot.all.count} plots for #{@@plots_houses.count} entries in @@plots_houses"
    puts "Created #{House.all.count} houses for #{@@plots_houses.count} entries in @@plots_houses"
  end

  @@users = [
    {name: 'Kartik Krishnan',     email: 'Kartik@gmail.com'},
    {name: 'Mahadevan',           email: 'Mahadevan@gmail.com'},
    {name: 'Pallavi',             email: 'Pallavi@gmail.com'},
    {name: 'Abdul Rasheed',       email: 'Abdul@gmail.com'},
    {name: 'Kaushal Tenant',      email: 'Kaushal@hotmail.com'},
    {name: 'Krishna Tenant',      email: 'Krishna@hotmail.com'},
    {name: 'Shashidharan',        email: 'Shashidharan@gmail.com'},
    {name: 'ShashidharanWife',    email: 'ShashidharanWife@gmail.com'},
    {name: 'LordKrishna',         email: 'LordKrishna@gmail.com'},
    {name: 'LordKrishnaWife',     email: 'LordKrishnaWife@gmail.com'},
    {name: 'Natesh',              email: 'Natesh@gmail.com'},
    {name: 'Savitri',             email: 'Savitri@gmail.com'},
    {name: 'Natesh',              email: 'Natesh_1@gmail.com'},
    {name: 'Savitri',             email: 'Savitri_1@gmail.com'},
    {name: 'Shikaripura Tenant',  email: 'Shikaripura@hotmail.com'},
    {name: 'Rohini Tenant',       email: 'Rohini@hotmail.com'},
    {name: 'Jayawanth',           email: 'Jayawanth@gmail.com'},
    {name: 'Shikaripura',         email: 'Shikaripura@gmail.com'},
    {name: 'Rohini',              email: 'Rohini@gmail.com'},
    {name: 'German259 Tenant',    email: 'German259@hotmail.com'},
    {name: 'GermanWife259 Tenant',email: 'GermanWife259@hotmail.com'},
    {name: 'Nandish',             email: 'Nandish@gmail.com'},
    {name: 'Balaji Tenant',       email: 'Balaji@hotmail.com'},
    {name: 'Aparna Tenant',       email: 'Aparna@hotmail.com'},
    {name: 'Balaji',              email: 'Balaji@gmail.com'},
    {name: 'Aparna',              email: 'Aparna@gmail.com'},
    {name: 'Owner250',            email: 'Owner250@gmail.com'},
  ]

  def create_users
    User.delete_all
    puts "\nCreating Users ..."
    format_str = "| %-20s | %-30s |\n"
    printf format_str, 'Name', 'Email'

    @@users.each do |user, i|
      user.merge!({password: 'aaaa', password_confirmation: 'aaaa'})
      @user = User.create!(user)
      printf format_str, @user.name, @user.email
    end
    puts "\nCreated #{User.all.count} users for #{@@users.count} entries in @@users"
  end

  @@plot_owners = [
    { email: 'Kartik@gmail.com',          plot_number: '5'},
    { email: 'Mahadevan@gmail.com',       plot_number: '6'  },
    { email: 'Pallavi@gmail.com',         plot_number: '15' },
    { email: 'Abdul@gmail.com',           plot_number: '16'},
    { email: 'Shashidharan@gmail.com',    plot_number: '48/1'},
    { email: 'Natesh@gmail.com',          plot_number: '123'},
    { email: 'Jayawanth@gmail.com',       plot_number: '249'},
    { email: 'Owner250@gmail.com',        plot_number: '250'},
    { email: 'Shikaripura@gmail.com',     plot_number: '259'},
    { email: 'Nandish@gmail.com',         plot_number: '333'},
    { email: 'Balaji@gmail.com',          plot_number: '372'},
  ]

  def create_plot_owners
    PlotOwner.delete_all

    puts "\nCreating Plot Owners ..."
    s = "\t\t| %2s-%-6s| %-15s| %-20s | %-30s | %-8s|\n"
    printf s, 'Ph', 'Plot #', 'Plot Status', 'Name', 'Email', 'Area'

    @@plot_owners.each do |plot_owner|
      email, plot_number = plot_owner[:email], plot_owner[:plot_number]
      owner = get_plot_owner(plot_number)
      if owner
        puts " ERROR : Plot Owner already exists: #{owner.email}, Plot #: #{owner.plot_number}"
        next
      end
      user = User.find_by_email(email)
      plot = Plot.find_by_number(plot_number)
      if !user || !plot
        puts " ERROR : create_plot_owner: Cannot find User/Plot specified : #{plot_owner}"
        next
      end

      po = create_plot_owner(user, plot)
      if !po
        puts " ERROR : create_plot_owner failed : #{plot_owner} \n: #{user}\n: #{plot}"
        next
      end

      printf s, po.phase, po.plot_number, plot.status_text, po.name, po.email, po.plot_area
    end

    puts "Created #{PlotOwner.all.count} plot_owners for #{@@plot_owners.count} entries in @@plot_owners"
  end

@@house_owners = [
      { email: 'Kartik@gmail.com',          plot_number: '5',     house_number: '5'},
      { email: 'Abdul@gmail.com',           plot_number: '16',    house_number: '16'},
      { email: 'Shashidharan@gmail.com',    plot_number: '48/1',  house_number: '48/1A'},
      { email: 'LordKrishna@gmail.com',     plot_number: '48/1',  house_number: '48/1B'},
      { email: 'Natesh@gmail.com',          plot_number: '123',   house_number: '123A'},
      { email: 'Natesh_1@gmail.com',        plot_number: '123',   house_number: '123B'},
      { email: 'Jayawanth@gmail.com',       plot_number: '249',   house_number: '249'},
      { email: 'Shikaripura@gmail.com',     plot_number: '259',   house_number: '259'},
      { email: 'Nandish@gmail.com',         plot_number: '333',   house_number: '333'},
]

  def create_house_owners
    HouseOwner.delete_all

    puts "\nCreating House Owners ..."
    s = "\t\t| %2s-%-6s| %-15s| %-7s | %-15s |%-20s | %-30s | %-9s| %-10s| %-6s|\n"
    printf s, 'Ph', 'Plot #', 'Plot Status', 'House #', 'House Status', 'Name', 'Email', 'Plot Area', 'House Area', 'RWH'

    @@house_owners.each do |house_owner|
      email, plot_number, house_number = house_owner[:email], house_owner[:plot_number], house_owner[:house_number]
      owner = get_house_owner(house_number)
      if owner
        puts " ERROR : House Owner already exists : #{owner.email}, Plot #: #{owner.plot_number}, House #: #{owner.house_number}"
        next
      end
      user = User.find_by_email(email)
      plot = Plot.find_by_number(plot_number)
      house = House.find_by_number(house_number)
      if !user || !plot || !house
        puts " ERROR : create_house_owner: Cannot find User/Plot/House specified : #{house_owner}"
        next
      end

      pho = create_house_owner(user, house)
      if !pho
        puts " ERROR : create_house_owner failed : #{house_owner} \n: #{user}\n: #{plot}"
        next
      end
      printf s, pho.phase, pho.plot_number, plot.status_text, pho.house_number, house.status, pho.name, pho.email, pho.plot_area, pho.house_area, pho.rwh
    end

    puts "Created #{HouseOwner.all.count} house_owners for #{@@house_owners.count} entries in @@house_owners"
  end

  @@tenants = [
    {   email: 'Kaushal@hotmail.com',         plot_number: '16',   house_number: '16'     },
    {   email: 'Shikaripura@hotmail.com',     plot_number: '123',  house_number: '123A'   },
    {   email: 'German259@hotmail.com',       plot_number: '259',  house_number: '259'    },
    {   email: 'Balaji@hotmail.com',          plot_number: '333',  house_number: '333'    },
  ]

  def create_tenants
    Tenant.delete_all
    puts "\nCreating Tenants ..."
    s = "\t\t| %2s-%-6s| %-8s | %-20s | %-30s | %-6s|\n"
    printf s, 'Ph', 'Plot #', 'House #', 'Name', 'Email', 'RWH'

    @@tenants.each do |tenant|
      email, house_number = tenant[:email], tenant[:house_number]
      t = get_house_tenant(house_number)
      if t
        puts " ERROR : Tenant 't.email' already exists for House # #{house_number} in Plot # #{t.plot_number}"
        next
      end
      user = User.find_by_email(email)
      house = House.find_by_number(house_number)
      if !user || !house
        puts " ERROR : create_tenants: Cannot find User/House specified : #{tenant}"
        next
      end
      tenant = create_tenant(user, house)
      if !tenant
        puts " ERROR : create_tenant failed : #{tenant} \n: #{user}\n: #{house}"
        next
      end
      printf s, tenant.phase, tenant.plot_number, tenant.house_number, tenant.name, tenant.email, tenant.rwh
    end

    puts "Created #{Tenant.all.count} Tenants for #{@@tenants.count} entries in @@tenants"
  end

  @@owner_relatives = [
    {email: "Aparna@gmail.com", plot_number: "372"},
    {email: "Savitri@gmail.com", plot_number: "123"},
    {email: "Rohini@gmail.com", plot_number: "259"},
    {email: "LordKrishnaWife@gmail.com", house_number: "48/1B"},
  ]

  @@tenant_relatives = [
    {email: "Aparna@hotmail.com", house_number: "333"},
    {email: "Krishna@hotmail.com", house_number: "16"},
    {email: "Rohini@hotmail.com", house_number: "123A"},
    {email: "GermanWife259@hotmail.com", house_number: "259"},
  ]

  def create_relatives
    Relative.delete_all
    create_owner_relatives()
    create_tenant_relatives()
    puts "Created #{Relative.all.count} Relatives for - {#{@@owner_relatives.count} owners + #{@@tenant_relatives.count} tenants} entries in @@owner_relatives, @@tenant_relatives"
  end

  def create_owner_relatives
    puts "\nCreate Owner Relatives ..."
    s = "\t\t| %2s-%-6s| %-8s | %-20s | %-30s |\n"
    printf s, 'Ph', 'Plot #', 'House #', 'Name', 'Email'

    @@owner_relatives.each do |relative|
      email, plot_number, house_number  = relative[:email], relative[:plot_number], relative[:house_number]
      r = Relative.find_by_email(email)
      if r
        puts " ERROR : Relative '#{email}' already exists"
        next
      end
      user = User.find_by_email(email)
      plot = Plot.find_by_number(plot_number)
      house = House.find_by_number(house_number)
      if !user || (!plot && !house)
        puts " ERROR : create_relatives: Cannot find User, Plot/House specified in @@owner_relatives: #{relative}"
        next
      end
      if plot && house
        puts " ERROR : create_relatives: Cannot specify both Plot & House in @@owner_relatives: #{relative}"
        next
      end
      r = create_plot_owner_relative(user, plot) if plot
      r = create_house_owner_relative(user, house) if house
      if !r
        puts " ERROR : create_owner_relative failed : #{relative} \n: #{user}\n: #{plot}\n: #{house}"
        next
      end
      printf s, r.phase, r.plot_number, r.house_number, r.name, r.email
    end
  end

  def create_tenant_relatives
    puts "\nCreate Tenant Relatives ..."
    s = "\t\t| %2s-%-6s| %-7s | %-20s | %-30s |\n"
    printf s, 'Ph', 'Plot #', 'House #', 'Name', 'Email'

    @@tenant_relatives.each do |relative|
      email, house_number  = relative[:email], relative[:house_number]
      r = Relative.find_by_email(email)
      if r
        puts " ERROR : Relative '#{email}' already exists"
        next
      end
      user = User.find_by_email(email)
      house = House.find_by_number(house_number)
      if !user || !house
        puts " ERROR : create_tenant_relatives: Cannot find User, House specified in @@tenant_relatives: #{relative}"
        next
      end
      r = create_tenant_relative(user, house)
      if !r
        puts " ERROR : create_owner_relative failed : #{relative} \n: #{user}\n: #{house}"
        next
      end
      printf s, r.phase, r.plot_number, r.house_number, r.name, r.email
    end
  end
end

rbd_seed = RbdSeed.new

# Create domain data
rbd_seed.create_plots_and_houses()
rbd_seed.create_users()

# STI models
Member.delete_all # cleanup all members
rbd_seed.create_plot_owners()
rbd_seed.create_house_owners()
rbd_seed.create_tenants()
rbd_seed.create_relatives()

require_relative 'print_tables'
# print all tables - 3 models and 6 relationships
print_tables = PrintTables.new

print_tables.print_plots
print_tables.print_users
print_tables.print_houses

# STI models
print_tables.print_plot_owners
print_tables.print_house_owners
print_tables.print_tenants
print_tables.print_relatives