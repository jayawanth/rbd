@domain_data
Feature: Admin Add domain data, Plots/Houses/Users - Success
  In order to allow only valid Plot, House, Users data to be added/deleted/updated,
  As an admin,
  I want the system to detect and reject invalid additions/deletions/updates whenever possible by
  crosschecking the data in Plots, Houses and Users tables as wells as other members tables.

# Plots can be empty or with one or more houses,
# Houses can be empty or owner occupied or rented,
# Users can be -
#   Owners: PlotOwners, HouseOwners (Primary if he owns the plot also otherwise Secondary)
#   Tenant or
#   Relatives of Owners or Tenants

  Background: plots, houses and users have been added to database

    Given these Plots exist in database:
      |phase|number   |status         |area     |
      | 1   | 5       | house         | 3544    |
      | 1   | 9       | house         | 4100    |
      | 1   | 11      | house         | 3375    |
      | 1   | 12      | house         | 3750    |
      | 1   | 13      | house         | 3000    |
      | 1   | 15      | empty         | 3750    |
      | 1   | 16      | house         | 3000    |
      | 1   | 123     | house         | 2400    |

    And these Houses exist in database:
      |phase|plot_number|number   |status          |area     | rwh  |
      | 1   | 5         | 5       | owner_occupied | 3544    | 0    |
      | 1   | 9         | 9       | owner_occupied | 4100    | 0    |
      | 1   | 11        | 11      | owner_occupied | 3375    | 0    |
      | 1   | 12        | 12      | owner_occupied | 3750    | 0    |
      | 1   | 13        | 13      | owner_occupied | 3000    | 0    |
      | 1   | 16        | 16      | rented         | 3000    | 0    |
      | 1   | 123       | 123A    | owner_occupied | 2400    | 0    |
      | 1   | 123       | 123B    | rented         | 2400    | 0    |

    And these Users exist in the database:
      | name                    | email                                 | password  | password_confirmation |
      | Kartik Krishnan		      | Kartik@gmail.com                      | aaaa      | aaaa                  |
      | Kartik Wife		          | KartikWife@gmail.com                  | aaaa      | aaaa                  |
      | Mahadevan			          | Mahadevan@gmail.com                   | aaaa      | aaaa                  |
      | Swami                   | Swami@gmail.com                       | aaaa      | aaaa                  |
      | Siva                    | Siva@gmail.com                        | aaaa      | aaaa                  |
      | Shanti                  | Shanti@gmail.com                      | aaaa      | aaaa                  |
      | Pallavi			            | Pallavi@gmail.com                     | aaaa      | aaaa                  |
      | PallaviHusband			    | PallaviHusband@gmail.com              | aaaa      | aaaa                  |
      | Abdul Rasheed			      | Abdul@gmail.com                       | aaaa      | aaaa                  |
      | Kaushal Tenant		      | Kaushal@hotmail.com                   | aaaa      | aaaa                  |
      | Krishna Tenant		      | Krishna@hotmail.com                   | aaaa      | aaaa                  |
      | Natesh			            | Natesh@gmail.com                      | aaaa      | aaaa                  |
      | Savitri			            | Savitri@gmail.com                     | aaaa      | aaaa                  |
      | Natesh			            | Natesh_1@gmail.com                    | aaaa      | aaaa                  |
      | Savitri			            | Savitri_1@gmail.com                   | aaaa      | aaaa                  |
      | Shikaripura Tenant	    | Shikaripura@hotmail.com               | aaaa      | aaaa                  |
      | Rohini Tenant			      | Rohini@hotmail.com                    | aaaa      | aaaa                  |
      | Shikaripura			        | Shikaripura@gmail.com                 | aaaa      | aaaa                  |
      | Rohini			            | Rohini@gmail.com                      | aaaa      | aaaa                  |

    And these Plot Owners exist in the database:
      | email                   | number |
      | Kartik@gmail.com        | 5      |
      | Pallavi@gmail.com       | 15     |
      | Abdul@gmail.com         | 16     |
      | Natesh@gmail.com        | 123    |

    And these House Owners exist in the database:
      | email                   | plot_number | number  |
      | Kartik@gmail.com        | 5           | 5       |
      | Abdul@gmail.com         | 16          | 16      |
      | Natesh@gmail.com        | 123         | 123A    |
      | Natesh_1@gmail.com      | 123         | 123B    |

    And these Tenants exist in the database:
      | email                   | plot_number | house_number  |
      | Kaushal@hotmail.com     | 16          | 16            |
      | Shikaripura@hotmail.com | 123         | 123A          |

    And these Relatives exist in the database:
      | plot_number   | house_number  | email                    | user_type      |
      |               | 5             | KartikWife@gmail.com     | HouseOwner     |
      | 15            |               | PallaviHusband@gmail.com | PlotOwner      |
      |               | 16            | Krishna@hotmail.com      | Tenant         |

    And  I am on the Rbd home page

  @plots @plot_success
  Scenario Outline: Create Plots

    Given I am on the Plots page
    And I follow "Add new plot"
    When I create a new Plot in phase "<phase>" with number "<number>", status "<status>", area "<area>"
    Then I should see action "Created" for entity "<entity>" with number "<number>"

  Examples: Success cases
    |phase|number   |status         |area     | entity  |
    | 4   | 429     | house         | 4000    | Plot    |
    | 4   | 397     | house         | 4800    | Plot    |

  @plots @plot_failures
  Scenario Outline: Create invalid Plots

    Given I am on the Plots page
    And I follow "Add new plot"
    When I create a new Plot in phase "<phase>" with number "<number>", status "<status>", area "<area>"
    Then I shouldn't see action "Created" for entity "<entity>" with number "<number>"
    And I should see action "Create: Error" for number "<number>"

  # 1 - Error, plot already exists
  # 2 - Error, invalid plot #
  # 3 - Error, invalid area
  # 4 - Error, area out of range

  Examples: Failure cases
    | Sl. No  |phase |number    |status         |area      | entity  |
    | 1       | 1    | 5        | empty         | 3544     | Plot    |
    | 2       | 1    | number?  | house         | 4800     | Plot    |
    | 3       | 1    | 5A       | house         | area?    | Plot    |
    | 4       | 1    | 5A       | house         | 1        | Plot    |
    | 4       | 1    | 5A       | house         | 12000    | Plot    |

  @houses @house_success
  Scenario Outline: Create Houses

    Given I am on the Houses page
    And I follow "Add new house"
    When I create a new House in phase-plot "<phase-plot>", with number "<number>", status "<status>", area "<area>", rwh "<rwh>"
    Then I should see action "Created" for entity "<entity>" with number "<number>"

  Examples: Success cases
    |phase-plot |number   |status           |area     | entity  | rwh   |
    | 1-5       | 5A       | owner_occupied  | 2000    | House   |  0    |
    | 1-9       | 9A       | owner_occupied  | 2400    | House   |  0    |

  @houses @house_failures
  Scenario Outline: Create invalid Houses

    Given I am on the Houses page
    And I follow "Add new house"
    When I create a new House in phase-plot "<phase-plot>", with number "<number>", status "<status>", area "<area>", rwh "<rwh>"
    Then I shouldn't see action "Created" for entity "<entity>" with number "<number>"
    And I should see action "Create: Error" for number "<number>"

  # 1 - Error, house already exists
  # 2 - Error, invalid plot #
  # 3 - Error, invalid area
  # 4 - Error, area out of range

  Examples: Failure cases
    | Sl. No  |phase-plot |number   |status         |area      | entity  | rwh  |
    | 1       | 1-5       | 5       | owner_occupied| 3544     | House   | 0    |
    | 2       | 1-9       | number? | owner_occupied| 4800     | House   | 0    |
    | 3       | 1-5       | 5A      | owner_occupied| area?    | House   | 0    |
    | 4       | 1-5       | 5A      | owner_occupied| 1        | House   | 0    |

  @users @user_success
  Scenario Outline: Create Users

    Given I am on the Users page
    And I follow "Add new user"
    When I create a new User with email "<email>", name "<name>", password "<password>", password_confirmation "<password>"
    Then I should see action "Created" for entity "<entity>" with email "<email>"

  Examples: Success cases
    | email             | name              |password | entity  |
    | Joe@gmail.com     | Joe Kamicheril    | aaaa    | User    |
    | Anand@gmail.com   | Anand Patti       | aaaa    | User    |

  @users @user_failures
  Scenario Outline: Create invalid Users

    Given I am on the Users page
    And I follow "Add new user"
    When I create a new User with email "<email>", name "<name>", password "<password>", password_confirmation "<password>"
    Then I shouldn't see action "Created" for entity "<entity>" with email "<email>"
    And I should see action "Create: Error" for email "<email>"

  Examples: Failure cases
    | email                 | name              |password         | entity  |
    | Kartik@gmail.com      | Kartik Krishnan   | aaaa            | User    |
    | email?                | Anand Patti       | aaaa            | User    |
    | Joe@gmail.com         | Joe Kamicheril    | xx              | User    |
    | Joe@gmail.com         | Joe Kamicheril    | 0123456789012   | User    |

