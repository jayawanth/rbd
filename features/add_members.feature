@members
Feature: Admin Add PlotOwners/HouseOwners/Tenants/Relatives
  In order to allow only valid users to be added as PlotOwners/HouseOwners/Tenants/Relatives,
  As an admin,
  I want the system to detect and reject invalid additions whenever possible by
  crosschecking the data in Plots, Houses and Users tables.

# Plots can be empty or with one or more houses,
# Houses can be empty or owner occupied or rented,
# Users can be -
#   Owners: PlotOwners, HouseOwners (Primary if he owns the plot also otherwise Secondary)
#   Tenant or
#   Relatives of Owners or Tenants

  Background: Some Plots/Houses/Users, PlotOwners/HouseOwners/Tenants/Relatives have been added to database

    Given these Plots exist in database:
      |phase|number   |status         |area     |
      | 1   | 5       | house         | 3544    |
      | 1   | 9       | house         | 4100    |
      | 1   | 11      | house         | 3375    |
      | 1   | 12      | house         | 3750    |
      | 1   | 13      | house         | 3000    |
      | 1   | 15      | empty         | 3750    |
      | 1   | 16      | house         | 3000    |
      | 1   | 21      | house         | 4950    |
      | 1   | 25      | house         | 4500    |
      | 1   | 41      | empty         | 2400    |
      | 1   | 42      | house         | 2700    |
      | 1   | 48/1    | house         | 2500    |
      | 1   | 49/1    | house         | 1500    |
      | 1   | 54      | house         | 2400    |
      | 1   | 56      | house         | 2250    |
      | 1   | 62      | house         | 2250    |
      | 1   | 64      | house         | 3194    |
      | 1   | 70/1    | house         | 3492    |
      | 1   | 123     | house         | 2400    |
      | 1   | 130     | house         | 8026    |
      | 2   | 143/1   | house         | 3211    |
      | 2   | 157     | house         | 2504    |
      | 3   | 242     | house         | 2546    |
      | 3   | 247     | house         | 3618    |
      | 3   | 248     | house         | 4484    |
      | 3   | 249     | house         | 5366    |
      | 3   | 251     | house         | 2000    |
      | 3   | 259     | house         | 2509    |
      | 3   | 261     | house         | 3584    |
      | 3   | 274     | house         | 3500    |
      | 3   | 281     | house         | 3666    |
      | 3   | 282     | house         | 4000    |
      | 3   | 333     | house         | 2055    |
      | 3   | 359     | house         | 2397    |
      | 4   | 372     | construction  | 4000    |
      | 4   | 429     | house         | 4000    |

    And these Houses exist in database:
      |phase|plot_number|number   |status          |area     | rwh  |
      | 1   | 5         | 5       | owner_occupied | 3544    | 0    |
      | 1   | 9         | 9       | owner_occupied | 4100    | 0    |
      | 1   | 11        | 11      | owner_occupied | 3375    | 0    |
      | 1   | 12        | 12      | owner_occupied | 3750    | 0    |
      | 1   | 13        | 13      | owner_occupied | 3000    | 0    |
      | 1   | 16        | 16      | rented         | 3000    | 0    |
      | 1   | 21        | 21      | owner_occupied | 4950    | 0    |
      | 1   | 25        | 25      | owner_occupied | 4500    | 0    |
      | 1   | 42        | 42      | owner_occupied | 2700    | 0    |
      | 1   | 48/1      | 48/1A   | owner_occupied | 2500    | 0    |
      | 1   | 48/1      | 48/1B   | owner_occupied | 2500    | 0    |
      | 1   | 49/1      | 49/1    | owner_occupied | 1500    | 0    |
      | 1   | 54        | 54      | owner_occupied | 2400    | 0    |
      | 1   | 56        | 56      | owner_occupied | 2250    | 0    |
      | 1   | 62        | 62/B    | owner_occupied | 1500    | 0    |
      | 1   | 64        | 64      | rented         | 3194    | 0    |
      | 1   | 70/1      | 70/1    | owner_occupied | 3492    | 0    |
      | 1   | 123       | 123A    | owner_occupied | 2400    | 0    |
      | 1   | 123       | 123B    | rented         | 2400    | 0    |
      | 1   | 130       | 130     | owner_occupied | 8026    | 0    |
      | 2   | 143/1     | 143/1   | owner_occupied | 3211    | 0    |
      | 2   | 157       | 157     | owner_occupied | 2504    | 0    |
      | 3   | 242       | 242     | owner_occupied | 2546    | 0    |
      | 3   | 247       | 247     | owner_occupied | 3618    | 0    |
      | 3   | 248       | 248     | owner_occupied | 4484    | 0    |
      | 3   | 249       | 249     | owner_occupied | 5366    | 0    |
      | 3   | 251       | 251     | owner_occupied | 2000    | 0    |
      | 3   | 259       | 259     | rented         | 2509    | 0    |
      | 3   | 261       | 261     | owner_occupied | 3584    | 0    |
      | 3   | 274       | 274     | owner_occupied | 3500    | 0    |
      | 3   | 281       | 281     | rented         | 3666    | 0    |
      | 3   | 282       | 282     | owner_occupied | 4000    | 0    |
      | 3   | 333       | 333     | rented         | 2055    | 0    |
      | 3   | 359       | 359     | owner_occupied | 2397    | 0    |
      | 4   | 429       | 429     | owner_occupied | 4000    | 0    |

    And these Users exist in the database:
      | name                    | email                                 | password  | password_confirmation |
      | Kartik Krishnan		      | Kartik@gmail.com                      | aaaa      | aaaa                  |
      | Kartik Wife		          | KartikWife@gmail.com                  | aaaa      | aaaa                  |
      | Mahadevan			          | Mahadevan@gmail.com                   | aaaa      | aaaa                  |
      | Swami                   | Swami@gmail.com                       | aaaa      | aaaa                  |
      | Siva                    | Siva@gmail.com                        | aaaa      | aaaa                  |
      | Shanti                  | Shanti@gmail.com                      | aaaa      | aaaa                  |
      | Pallavi			            | Pallavi@gmail.com                     | aaaa      | aaaa                  |
      | PallaviHusband			    | PallaviHusband@gmail.com              | aaaa      | aaaa                  |
      | Abdul Rasheed			      | Abdul@gmail.com                       | aaaa      | aaaa                  |
      | Kaushal Tenant		      | Kaushal@hotmail.com                   | aaaa      | aaaa                  |
      | Krishna Tenant		      | Krishna@hotmail.com                   | aaaa      | aaaa                  |
      | Shashidharan			      | Shashidharan@gmail.com                | aaaa      | aaaa                  |
      | ShashidharanWife		    | ShashidharanWife@gmail.com            | aaaa      | aaaa                  |
      | LordKrishna			        | LordKrishna@gmail.com                 | aaaa      | aaaa                  |
      | LordKrishnaWife		      | LordKrishnaWife@gmail.com             | aaaa      | aaaa                  |
      | Natesh			            | Natesh@gmail.com                      | aaaa      | aaaa                  |
      | Savitri			            | Savitri@gmail.com                     | aaaa      | aaaa                  |
      | Natesh			            | Natesh_1@gmail.com                    | aaaa      | aaaa                  |
      | Savitri			            | Savitri_1@gmail.com                   | aaaa      | aaaa                  |
      | Shikaripura Tenant	    | Shikaripura@hotmail.com               | aaaa      | aaaa                  |
      | Rohini Tenant			      | Rohini@hotmail.com                    | aaaa      | aaaa                  |
      | Shikaripura			        | Shikaripura@gmail.com                 | aaaa      | aaaa                  |
      | Rohini			            | Rohini@gmail.com                      | aaaa      | aaaa                  |
      | German259 Tenant		    | German259@hotmail.com                 | aaaa      | aaaa                  |
      | GermanWife259 Tenant    | GermanWife259@hotmail.com             | aaaa      | aaaa                  |
      | Nandish			            | Nandish@gmail.com                     | aaaa      | aaaa                  |
      | Balaji Tenant			      | Balaji@hotmail.com                    | aaaa      | aaaa                  |
      | Aparna Tenant			      | Aparna@hotmail.com                    | aaaa      | aaaa                  |
      | Balaji			            | Balaji@gmail.com                      | aaaa      | aaaa                  |
      | Aparna			            | Aparna@gmail.com                      | aaaa      | aaaa                  |
      | Owner250			          | Owner250@gmail.com                    | aaaa      | aaaa                  |
      | Lalith                  | Lalith@gmail.com                      | aaaa      | aaaa                  |
      | Raju                    | Raju@gmail.com                        | aaaa      | aaaa                  |
      | Jacob                   | Jacob@gmail.com                       | aaaa      | aaaa                  |
      | Phillip                 | Phillip@hotmail.com                   | aaaa      | aaaa                  |
      | Nicole                  | Nicole@hotmail.com                    | aaaa      | aaaa                  |

    And these Plot Owners exist in the database:
      | email                   | number |
      | Kartik@gmail.com        | 5      |
      | Pallavi@gmail.com       | 15     |
      | Abdul@gmail.com         | 16     |
      | Natesh@gmail.com        | 123    |

    And these House Owners exist in the database:
      | email                   | plot_number | number  |
      | Kartik@gmail.com        | 5           | 5       |
      | Abdul@gmail.com         | 16          | 16      |
      | Natesh@gmail.com        | 123         | 123A    |
      | Natesh_1@gmail.com      | 123         | 123B    |
      | Shikaripura@gmail.com   | 259         | 259     |

    And these Tenants exist in the database:
      | email                   | plot_number | house_number  |
      | Kaushal@hotmail.com     | 16          | 16            |
      | Shikaripura@hotmail.com | 123         | 123A          |
      | Phillip@hotmail.com     | 281         | 281           |

    And these Relatives exist in the database:
      | plot_number   | house_number  | email                    | user_type      |
      |               | 5             | KartikWife@gmail.com     | HouseOwner     |
      | 15            |               | PallaviHusband@gmail.com | PlotOwner      |
      |               | 16            | Krishna@hotmail.com      | Tenant         |

    And  I am on the Rbd home page

  @plot_owners @plot_owner_success
  Scenario Outline: Create PlotOwners

    Given I am on the Plot Owners page
    And I follow "Add new plot_owner"
    When I create a new Plot Owner with email "<email>" for plot "<Plot>"
    Then I should see action "Created" user of type "<user_type>" with email "<email>"

  Examples: Success cases
    | Plot              | email                     | user_type   |
    | 9                 | Swami@gmail.com           | Plot Owner  |
    | 48/1              | Shashidharan@gmail.com    | Plot Owner  |
    | 259               | Shikaripura@gmail.com     | Plot Owner  |
    | 333               | Nandish@gmail.com         | Plot Owner  |
    | 372               | Balaji@gmail.com          | Plot Owner  |

  @plot_owners @plot_owner_failures
  Scenario Outline: Create invalid PlotOwners

    Given I am on the Plot Owners page
    And I follow "Add new plot_owner"
    When I create a new Plot Owner with email "<email>" for plot "<Plot>"
    Then I shouldn't see action "Created" user of type "<user_type>" with email "<email>"
    And I should see action "Create: Error" for email "<email>"

  # 1 - Error, valid plot owner already exists for Plot #5
  # 2 - Error, invalid plot # 999, valid user
  # 3 - Error, valid plot #, invalid user
  Examples: Failure cases
    | Sl. No  | Plot   | email                      | user_type   |
    | 1       | 5      | Jayawanth@gmail.com        | Plot Owner  |
    | 2       | 999    | Kartik@gmail.com           | Plot Owner  |
    | 3       | 5      | unknown@gmail.com          | Plot Owner  |

  @house_owners @house_owner_success
  Scenario Outline: Create HouseOwners

    Given I am on the House Owners page
    And I follow "Add new house_owner"
    When I create a new House Owner with email "<email>" for house "<Plot-House>"
    Then I should see action "Created" user of type "<user_type>" with email "<email>"

  Examples: Success cases
    | Plot-House  | email                     | user_type   |
    | 48/1-48/1A  | Shashidharan@gmail.com    | House Owner |
    | 48/1-48/1B  | LordKrishna@gmail.com     | House Owner |
    | 333-333     | Nandish@gmail.com         | House Owner |

  @house_owners @house_owner_failures
  Scenario Outline: Create invalid HouseOwners

    Given I am on the House Owners page
    And I follow "Add new house_owner"
    When I create a new House Owner with email "<email>" for house "<Plot-House>"
    Then I shouldn't see action "Created" user of type "<user_type>" with email "<email>"
    And I should see action "Create: Error" for email "<email>"

  # 1 - Error, valid house owner already exists for House #5
  # 2 - Error, invalid plot # 999, valid user
  # 3 - Error, valid plot #, invalid house #, valid user
  # 4 - Error, valid plot & house, invalid user
  Examples: Failure cases
    | Plot-House | email                      | user_type   |
    | 5-5        | Jayawanth@gmail.com        | House Owner |
    | 999-5      | Kartik@gmail.com           | House Owner |
    | 5-999      | Kartik@gmail.com           | House Owner |
    | 5-5        | unknown@gmail.com          | House Owner |

  @tenants @tenant_success
  Scenario Outline: Create Tenants

    Given I am on the Tenants page
    And I follow "Add new tenant"
    When I create a new Tenant with email "<email>" for house "<Plot-House>"
    Then I should see action "Created" user of type "<user_type>" with email "<email>"

  Examples: Success cases
    | Plot-House  | email                     | user_type |
    | 259-259     | German259@hotmail.com     | Tenant    |

  @tenants @tenant_failures
  Scenario Outline: Create invalid Tenants

    Given I am on the Tenants page
    And I follow "Add new tenant"
    When I create a new Tenant with email "<email>" for house "<Plot-House>"
    Then I shouldn't see action "Created" user of type "<user_type>" with email "<email>"
    And I should see action "Create: Error" for email "<email>"

  # 1 - Error, valid tenant already exists for House #16
  # 2 - Error, invalid plot # 999, valid user
  # 3 - Error, valid plot #, invalid house #, valid user
  # 4 - Error, valid plot & house, invalid user
  Examples: Failure cases
    | Plot-House | email                      | user_type   |
    | 16-16      | Jayawanth@gmail.com        | Tenant      |
    | 999-5      | Kartik@gmail.com           | Tenant      |
    | 5-999      | Kartik@gmail.com           | Tenant      |
    | 5-5        | unknown@gmail.com          | Tenant      |

  @relatives @relative_success
  Scenario Outline: Create Relatives

    Given I am on the Relatives page
    And I follow "Add new relative"
    When I create a new Relative of type "<user_type>" with email "<email>" for entity "<number>"
    Then I should see action "Created" user of type "<user_type_text>" with email "<email>"

  Examples: Success cases
    | number  | email                       | user_type      | user_type_text              |
    | 123     | Savitri@gmail.com           | PlotOwner      | Relative of PlotOwner       |
    | 259     | Rohini@gmail.com            | HouseOwner     | Relative of HouseOwner      |
    | 281     | Nicole@hotmail.com          | Tenant         | Relative of Tenant          |

  @relatives @relative_failures
  Scenario Outline: Create Relatives

    Given I am on the Relatives page
    And I follow "Add new relative"
    When I create a new Relative of type "<user_type>" with email "<email>" for entity "<number>"
    Then I shouldn't see action "Created" user of type "<user_type_text>" with email "<email>"
    And I should see action "Create: Error" for email "<email>"

  # 1, 2, 3 - Error, adding an existing Relative of Plot/HouseOwner/Tenant
  # 4 - Error, invalid plot # 999, valid user
  # 5 - Error, invalid house #, valid user
  # 6 - Error, invalid user
  Examples: Failure cases
    | number      | email                    | user_type    |  user_type_text          |
    | 15          | PallaviHusband@gmail.com | PlotOwner    | Relative of PlotOwner    |
    | 5           | KartikWife@gmail.com     | HouseOwner   | Relative of HouseOwner   |
    | 16          | Krishna@hotmail.com      | Tenant       | Relative of Tenant       |
    | 999         | Kartik@gmail.com         | PlotOwner    | Relative of PlotOwner    |
    | 999         | Kartik@gmail.com         | HouseOwner   | Relative of HouseOwner   |
    | 5           | unknown@gmail.com        | HouseOwner   | Relative of HouseOwner   |
