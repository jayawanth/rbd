# Add a declarative step here for populating the DB with plots, houses, users.

Given /these Plots exist in database/ do |plots_table|
  plots_table.hashes.each do |plot|
    Plot.create!(plot)
  end
end

And /these Houses exist in database/  do |houses_table|
  houses_table.hashes.each do |house|
    plot = Plot.find_by_number(house[:plot_number])
    if !plot
      puts "user_steps.rb: houses_table initialization: Plot #{house[:plot_number]} not found"
    else
      house[:plot_id] = plot.id
      house = House.create!(house)
    end
  end
end

And /these Users exist in the database:/ do |users_table|
  users_table.hashes.each do |user|
    @user = User.create!(user)
  end
end

And /these Plot Owners exist in the database:/  do |plot_owners_table|
  plot_owners_table.hashes.each do |owner|
    user = User.find_by_email(owner[:email])
    plot = Plot.find_by_number(owner[:number])
    if !plot || !user
      puts "user_steps.rb: plot_owners_table initialization: user/plot not found - #{owner}"
    else
      po = create_plot_owner(user, plot)
    end
  end
end

And /these House Owners exist in the database:/  do |house_owners_table|
  house_owners_table.hashes.each do |owner|
    user = User.find_by_email(owner[:email])
    plot = Plot.find_by_number(owner[:plot_number])
    house = House.find_by_number(owner[:number])
    if !user || !plot || !house
      puts "user_steps.rb: house_owners_table initialization: user/plot/house not found - #{owner}"
    else
      ho = create_house_owner(user, house)
    end
  end
end

And /these Tenants exist in the database:/  do |tenants_table|
  tenants_table.hashes.each do |tenant|
    user = User.find_by_email(tenant[:email])
    plot = Plot.find_by_number(tenant[:plot_number])
    house = House.find_by_number(tenant[:house_number])
    if !user || !plot || !house
      puts "user_steps.rb: tenants initialization: user/plot/house not found - #{tenant}"
    else
      t = create_tenant(user, house)
    end
  end
end

And /these Relatives exist in the database:/  do |relatives_table|
  relatives_table.hashes.each do |relative|
    user = User.find_by_email(relative[:email])
    plot = Plot.find_by_number(relative[:plot_number])
    house = House.find_by_number(relative[:house_number])
    user_type = relative[:user_type]
    if !user || (!plot && !house)
      puts "user_steps.rb: relatives initialization: user/plot/house not found - #{relative}"
      next
    end

    r = nil
    r = create_plot_owner_relative(user, plot) if user_type == "PlotOwner" && plot
    r = create_house_owner_relative(user, house) if user_type == "HouseOwner" && house
    r = create_tenant_relative(user, house) if user_type == "Tenant" && house
    if !r
      puts " ERROR : create relative failed : #{relative}"
      next
    end
  end
end
