When /^I create a new Plot in phase "([^"]+)" with number "([^"]+)", status "([^"]+)", area "([^"]+)"$/  do |phase, number, status, area|
  choose "plot_phase_#{phase}"
  fill_in "plot_number",        with: number
  choose "plot_status_#{status}"
  fill_in "plot_area",          with: area
  click_button "Create Plot"
end

When /^I create a new House in phase-plot "([^"]+)\-([^"]+)", with number "([^"]+)", status "([^"]+)", area "([^"]+)", rwh "([^"]+)"$/  do |phase, plot_number, house_number, status, area, rwh|
  choose "house_phase_#{phase}"
  fill_in "house_plot_number",    with: plot_number
  fill_in "house_number",         with: house_number
  choose "house_status_#{status}"
  fill_in "house_area",           with: area
  fill_in "house_rwh",            with: rwh
  click_button "Create House"
end

When /^I create a new User with email "([^"]+)", name "([^"]+)", password "([^"]+)", password_confirmation "([^"]+)"$/  do |email, name, password, password_confirmation|
  fill_in "user_name",        with: name
  fill_in "user_email",       with: email
  fill_in "user_password",              with: password
  fill_in "user_password_confirmation", with: password_confirmation
  click_button "Create User"
end


Then /^I should(n't)? see action "([^"]+)" for entity "([^"]+)" with number "([^"]+)"$/  do |absence, action, entity, number|
  reg_exp = /#{action}\s+#{entity}.*#{number}/im
  if absence
    page.body.should_not =~ reg_exp
  else
    page.body.should =~ reg_exp
  end
  # TBD, check tag and content
end

Then /^I should(n't)? see action "([^"]+)" for number "([^"]+)"$/  do |absence, action, number|
  reg_exp = /#{action}.*#{number}/im
  if absence
    page.body.should_not =~ reg_exp
  else
    page.body.should =~ reg_exp
  end
  # TBD, check tag and content
end

Then /^I should(n't)? see action "([^"]+)" for entity "([^"]+)" with email "([^"]+)"$/  do |absence, action, entity, email|
  reg_exp = /#{action}\s+#{entity}.*#{email}/im
  if absence
    page.body.should_not =~ reg_exp
  else
    page.body.should =~ reg_exp
  end
  # TBD, check tag and content
end

Then /^I should(n't)? see action "([^"]+)" for email "([^"]+)"$/  do |absence, action, email|
  reg_exp = /#{action}.*#{email}/im
  if absence
    page.body.should_not =~ reg_exp
  else
    page.body.should =~ reg_exp
  end
  # TBD, check tag and content
end

