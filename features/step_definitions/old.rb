#When I create a new <user_description> with <family_status>, <name> in <Phase-Plot-House>, <email>
When /I create a new "([^"]*)" with "([^"]*)", "([^"]*)" in "(\d)\-([^"]+)\-([^"]*)", "([^"]*)"$/ do |user_description, family_status, name, phase, plot, house, email|
  fill_in "user_name",                 with: name
  fill_in "user_email",                with: (email.empty? == true)? "#{name}@gmail.com": email
  user_type = user_description.split.join('_')
  choose "user_user_type_#{user_type}"
  if family_status =~ /^family\s+member$/i
#  if family_status == "family member"
    choose "user_family_member_true"
  else # if family_status == "family head"
    choose "user_family_member_false"
  end
  choose "user_phase_#{phase}"
  fill_in "user_plot_number",          with: plot
  fill_in "user_house_number",         with: house
  fill_in "user_password",             with: "aaaa"
  fill_in "user_password_confirmation",with: "aaaa"
  click_button "Create User"
end

When /I create a new "([^"]*)" "([^"]*)" with name "([^"]*)" in Phase-Plot-House "(\d)\-([^"]+)\-([^"]*)", email "([^"]*)"$/ do |user_description, family_status, name, phase, plot, house, email|
  fill_in "user_name",                 with: name
  fill_in "user_email",                with: (email.empty? == true)? "#{name}@gmail.com": email
  user_type = user_description.split.join('_')
  choose "user_user_type_#{user_type}"
  if family_status =~ /^family\s+member$/i
    choose "user_family_member_true"
  else # if family_status == "family head"
    choose "user_family_member_false"
  end
  choose "user_phase_#{phase}"
  fill_in "user_plot_number",          with: plot
  fill_in "user_house_number",         with: house
  fill_in "user_password",             with: "aaaa"
  fill_in "user_password_confirmation",with: "aaaa"
  click_button "Create User"
end

Then /^I should(n't)? see action "([^"]+)" user of type "([^"]+)" with email "([^"]+)"$/  do |absence, action, user_type, email|
  reg_exp = /#{action}\s+#{user_type}\s+#{email}/im
  if absence
    page.body.should_not =~ reg_exp
  else
    page.body.should =~ reg_exp
  end
  # TBD, check tag and content
end


Then /^I should(n't)? see "([^"]*)" as "([^"]*)" with approval$/  do |absence, email, user_type|
  reg_exp = /Created\b+#{user_type}.*#{email}/im
  if absence
    page.body.should_not =~ reg_exp
  else
    page.body.should =~ reg_exp
  end
  # TBD, check tag and content
end

