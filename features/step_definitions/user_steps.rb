When /I create a new Plot Owner with email "([^"]+)" for plot "([^"]+)"$/ do |email, plot|
  fill_in "plot_owner_email",         with: email
  fill_in "plot_owner_plot_number",   with: plot

  click_button "Create Plot owner"
end

When /I create a new House Owner with email "([^"]+)" for house "([^"]+)\-([^"]+)"$/ do |email, plot, house|
  fill_in "house_owner_email",         with: email
  fill_in "house_owner_plot_number",   with: plot
  fill_in "house_owner_house_number",  with: house

  click_button "Create House owner"
end

When /I create a new Tenant with email "([^"]+)" for house "([^"]+)\-([^"]+)"$/ do |email, plot, house|
  fill_in "tenant_email",         with: email
  fill_in "tenant_plot_number",   with: plot
  fill_in "tenant_house_number",  with: house

  click_button "Create Tenant"
end

When /I create a new Relative of type "([^"]+)" with email "([^"]+)" for entity "([^"]+)"$/ do |user_type, email, number|
  fill_in "relative_email",       with: email
  case user_type
    when "PlotOwner" then
      fill_in "relative_plot_number",   with: number
      choose "relative_member_type_plotowner"
    when "HouseOwner" then
      fill_in "relative_house_number", with: number
      choose "relative_member_type_houseowner"
    when "Tenant" then
      fill_in "relative_house_number", with: number
      choose "relative_member_type_tenant"
  end

  click_button "Create Relative"
end

Given /^I visit the edit user page for "([^"]+)"$/ do |user_email|
  user = User.find_by_email(user_email)
  visit edit_user_path(user)
end

And /^I update "([^"]+)" field with "([^"]+)"$/ do |field, value|
  fill_in "user_#{field}", with: value
  click_button "Update User"
  page.body
end

And /^I choose "([^"]+)" for field "([^"]+)" and update$/ do |value, field|
  choose "user_#{field}_#{value}"
  click_button "Update User"
  page.body
end

Then /^I should see "([^"]+)" "([^"]+)" for "([^"]+)"$/ do |action, outcome, user|
  reg_exp = /#{action}.*#{outcome}.*user/im
  page.body.should =~ reg_exp
  # TBD, check tag and content
end

