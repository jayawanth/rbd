FactoryGirl.define  do
  factory :user do
    sequence(:name) {|n| "user#{n}"}
    email { "#{name}@gmail.com" }
    password "aaaa"
    password_confirmation "aaaa"
  end

  factory :plot do
    phase "1"
    number "17"
    status "house"
    area "4500"
  end

  factory :house do
    phase "1"
    plot_number "17"
    number "17"
    status "owner_occupied"
    rwh "100"
    area "2000"
  end
end