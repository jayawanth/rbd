require "spec_helper"

RSpec::Matchers.define :allow_action do |*args|
  match do |permission|
    permission.allow?(*args).should be_true
  end
end

RSpec::Matchers.define :allow_action_set do |*args|
  match do |permission|
    controllers, actions = Array(args[0]), Array(args[1])
    controllers.each do |controller|
      actions.each do |action|
        permission.allow?(controller, action).should be_true
      end
    end
  end
end

describe Permission do
  context "as Guest"  do
    user = nil  # no user logged in, Visitor treated as a Guest
    subject {Permission.new(nil)}
    # allow Guest to visit all static pages
    it { should allow_action_set([:pages], [:home, :location])}

    # allow attempt to login - sessions#new, #create
    # allow anyone logged in to logout, sessions#destroy
    it { should allow_action_set([:sessions], [:new, :create, :destroy])}

    # Do not allow viewing/editing domain data or members data
    it { should_not allow_action([:users, :plots, :houses], [:show, :index, :new, :create, :edit, :update, :destroy])}
    it { should_not allow_action([:members, :owners, :plot_owners, :house_owners, :tenants, :relatives],
                                 [:show, :index, :new, :create, :edit, :update, :destroy])}
  end

  context "as Member"  do
    # Note 'create' works only inside let, not if it is in the outer most block
    # user = FactoryGirl.create(:user) # this works always
    let(:user) { create(:user) }

    subject {Permission.new(user)}
    # allow Guest to visit all static pages
    it { should allow_action_set([:pages], [:home, :location])}

    # allow attempt to login - sessions#new, #create
    # allow anyone logged in to logout, sessions#destroy
    it { should allow_action_set([:sessions], [:new, :create, :destroy])}

    # Allow viewing domain data or members data
    it { should allow_action_set([:users, :plots, :houses], [:show, :index])}
    it { should allow_action_set([:members, :owners, :plot_owners, :house_owners, :tenants, :relatives],
                                 [:show, :index])}
    # Do not allow editing domain data or members data
    it { should_not allow_action_set([:users, :plots, :houses], [:new, :create, :edit, :update, :destroy])}
    it { should_not allow_action_set([:members, :owners, :plot_owners, :house_owners, :tenants, :relatives],
                                 [:new, :create, :edit, :update, :destroy])}
  end

  context "as Admin"  do
    let(:user) { create(:user, name: "Jayawanth") }
    subject {Permission.new(user)}
    # allow Guest to visit all static pages
    it { should allow_action_set([:pages], [:home, :location])}

    # allow attempt to login - sessions#new, #create
    # allow anyone logged in to logout, sessions#destroy
    it { should allow_action_set([:sessions], [:new, :create, :destroy])}

    # Allow viewing & editing domain data or members data
    it { should allow_action_set([:users, :plots, :houses], [:show, :index, :new, :create, :edit, :update, :destroy])}
    it { should allow_action_set([:members, :owners, :plot_owners, :house_owners, :tenants, :relatives],
                                 [:show, :index, :new, :create, :edit, :update, :destroy])}
  end

end