class OwnersController < ApplicationController
  helper_method :sort_column, :sort_direction

  # GET /owners
  # GET /owners.json
  def index
    @owners = Owner.order(sort_column + " " + sort_direction)
    @owners.count
  end

  # GET /owners/1
  # GET /owners/1.json
  def show
    @owner = Owner.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @owner }
    end
  end
  private
  def sort_column
    columns = Owner.column_names
    columns.include?(params[:sort]) ? params[:sort]: 'email'
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: 'asc'
  end
end
