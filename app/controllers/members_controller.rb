class MembersController < ApplicationController
  helper_method :sort_column, :sort_direction
  def show
    @member = Member.find(params[:id])
  end

  def index
    @members = Member.order(sort_column + " " + sort_direction)
  end

  private
  def sort_column
    columns = Member.column_names
    columns.include?(params[:sort]) ? params[:sort]: "email"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end
end
