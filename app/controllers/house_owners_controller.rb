require_relative 'helpers/members_helper'

class HouseOwnersController < ApplicationController
  helper_method :sort_column, :sort_direction
  include MembersHelper

  def new
    @house_owner = HouseOwner.new
  end

  def show
    @house_owner = HouseOwner.find(params[:id])
  end

  def create
    email, plot_number, house_number = params[:house_owner][:email], params[:house_owner][:plot_number], params[:house_owner][:house_number]
    owner = get_house_owner(house_number)
    if owner
      flash[:alert] = "HouseOwner::Create: Error - #{email}. House Owner already exists for #{house_number}: #{owner.email}"
      @house_owner = HouseOwner.new
      render 'new' and return
    end
    user = User.find_by_email(email)
    plot = Plot.find_by_number(plot_number)
    house = House.find_by_number(house_number)
    if !user || !plot || !house
      flash[:alert] = "Create: Error - Specified email/plot-house is invalid - #{email}, #{plot_number}-#{house_number}"
      @house_owner = HouseOwner.new
      render 'new' and return
    end

    @house_owner = create_house_owner(user, house)
    flash[:notice] = "Created House Owner #{@house_owner.email}"
    # session[:user_id] = @user.id   # Login the user too
    redirect_to house_owners_path
  end

  def index
    @house_owners = HouseOwner.order(sort_column + " " + sort_direction)
  end

  def destroy
    @house_owner = HouseOwner.find(params[:id])
    flash[:notice] = "HouseOwner '#{@house_owner.email}' deleted."
    @house_owner.destroy
    redirect_to house_owners_path
  end

  private
  def sort_column
    columns = HouseOwner.column_names
    columns.include?(params[:sort]) ? params[:sort]: "email"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end

end
