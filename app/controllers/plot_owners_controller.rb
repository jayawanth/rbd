require_relative 'helpers/members_helper'

class PlotOwnersController < ApplicationController
  helper_method :sort_column, :sort_direction
  include MembersHelper

  def new
    @plot_owner = PlotOwner.new
  end

  def show
    @plot_owner = PlotOwner.find(params[:id])
  end

  def create
    email, plot_number = params[:plot_owner][:email], params[:plot_owner][:plot_number]
    owner = get_plot_owner(plot_number)
    if owner
      flash[:alert] = "PlotOwner::Create: Error - #{email}. Plot Owner #{owner.email} already exists for #{plot_number}"
      @plot_owner = PlotOwner.new
      render 'new' and return
    end
    user = User.find_by_email(email)
    plot = Plot.find_by_number(plot_number)
    if !user || !plot
      flash[:alert] = "Create: Error - Specified email or plot # is invalid - #{email}, #{plot_number}"
      @plot_owner = PlotOwner.new
      render 'new' and return
    end

    @plot_owner = create_plot_owner(user, plot)
    flash[:notice] = "Created Plot Owner #{@plot_owner.email}"
    # session[:user_id] = @user.id   # Login the user too
    redirect_to plot_owners_path
  end

  def index
    @plot_owners = PlotOwner.order(sort_column + " " + sort_direction)
  end

  def destroy
    @plot_owner = PlotOwner.find(params[:id])
    flash[:notice] = "PlotOwner '#{@plot_owner.email}' deleted."
    @plot_owner.destroy
    redirect_to plot_owners_path
  end

  private
  def sort_column
    columns = PlotOwner.column_names
    columns.include?(params[:sort]) ? params[:sort]: "email"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end

  def current_resource
    @current_resource ||= PlotOwner.find(params[:id]) if params[:id]
  end
end
