class PlotsController < ApplicationController
  helper_method :sort_column, :sort_direction

  def new
    @plot = Plot.new
  end

  def create
    plot = params[:plot]
    @plot = Plot.new(plot)
    if !@plot.save
      flash[:alert] = "Plot::Create: Error - Plot # #{plot[:number]}."
      @plot = Plot.new
      render 'new' and return
    end
    flash[:notice] = "Created Plot # #{@plot.number}."
    redirect_to plots_url
  end

  def index
    @plots = Plot.order(sort_column + " " + sort_direction)
  end

  def show
    @plot = Plot.find(params[:id])
  end

  def edit
    @plot = Plot.find params[:id]
  end

  def update
    @plot = Plot.find params[:id]
    @plot.update_attributes!(params[:plot])
    flash[:notice] = "Plot # #{@plot.number} was successfully updated."
    redirect_to plot_path(@plot)
  end

  def destroy
    @plot = Plot.find(params[:id])
    @plot.destroy
    flash[:notice] = "Plot '#{@plot.number}' deleted."
    redirect_to plots_path
  end

private
  def sort_column
    columns = Plot.column_names
    columns.include?(params[:sort]) ? params[:sort]: "phase"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end
end
