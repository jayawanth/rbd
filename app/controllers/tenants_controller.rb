require_relative 'helpers/members_helper'

class TenantsController < ApplicationController
  helper_method :sort_column, :sort_direction
  include MembersHelper

  def new
    @tenant = Tenant.new
  end

  def show
    @tenant = Tenant.find(params[:id])
  end

  def create
    email, plot_number, house_number = params[:tenant][:email], params[:tenant][:plot_number], params[:tenant][:house_number]
    t = get_house_tenant(house_number)
    if t
      flash[:alert] = "Tenant::Create: Error - #{email}. Tenant #{t.email} already exists for #{house_number}"
      @tenant = Tenant.new
      render 'new' and return
    end
    user = User.find_by_email(email)
    plot = Plot.find_by_number(plot_number)
    house = House.find_by_number(house_number)
    if !user || !plot || !house || house.status != "rented"
      flash[:alert] = "Tenant::Create: Error - Specified email/plot/house is invalid - #{email}, #{plot_number}-#{house_number}"
      @tenant = Tenant.new
      render 'new' and return
    end

    @tenant = create_tenant(user, house)

    flash[:notice] = "Created Tenant #{@tenant.email}"
    # session[:user_id] = @user.id   # Login the user too
    redirect_to tenants_path
  end

  def index
    @tenants = Tenant.order(sort_column + " " + sort_direction)
  end

  def destroy
    @tenant = Tenant.find(params[:id])
    flash[:notice] = "Tenant '#{@tenant.email}' deleted."
    @tenant.destroy
    redirect_to tenants_path
  end

  private
  def sort_column
    columns = Tenant.column_names
    columns.include?(params[:sort]) ? params[:sort]: "email"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end
end
