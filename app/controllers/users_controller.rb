class UsersController < ApplicationController
  helper_method :sort_column, :sort_direction

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    user = params[:user]
    @user = User.new(user)
    if !@user.save
      flash[:alert] = "User::Create: Error - User #{user[:email]}."
      @user = User.new
      render 'new' and return
    end
    flash[:notice] = "Created User #{@user.email}"
    # session[:user_id] = @user.id   # Login the user too
    redirect_to users_path
  end

  def index
    @users = User.order(sort_column + " " + sort_direction)
  end

  def edit
    @user = User.find params[:id]
  end

  def update
    @user = User.find params[:id]
    @user.update_attributes!(params[:user])
    flash[:notice] = "Updated user #{@user.email}"
    redirect_to user_path(@user)
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = "User '#{@user.email}' deleted."
    redirect_to users_path
  end

private
  def sort_column
    columns = User.column_names
    columns.include?(params[:sort]) ? params[:sort]: "email"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end
end
