module MembersHelper
def get_plot_owner(plot_number)
  PlotOwner.find_by_plot_number(plot_number)
end

def get_house_owner(house_number)
  HouseOwner.find_by_house_number(house_number)
end

def get_house_tenant(house_number)
  Tenant.find_by_house_number(house_number)
end

def get_relative(email)
  Relative.find_by_email(email)
end

def create_plot_owner(user, plot)
  plot_owner = PlotOwner.new
  copy_user_values(plot_owner, user)
  copy_plot_values(plot_owner, plot)
  plot_owner.save!
  plot_owner
end

def create_house_owner(user,house)
  house_owner = HouseOwner.new
  copy_user_values(house_owner, user)
  copy_plot_values(house_owner, house.plot)
  copy_house_values(house_owner, house)
  house_owner.save!
  house_owner
end

def create_tenant(user, house)
  tenant = Tenant.new
  copy_user_values(tenant, user)
  copy_plot_values(tenant, house.plot)
  copy_house_values(tenant, house)
  tenant.save!
  tenant
end

def create_plot_owner_relative(user, plot)
  relative = Relative.new
  copy_user_values(relative, user)
  copy_plot_values(relative, plot)
  relative.family = PlotOwner.find_by_plot_number(plot.number)
  relative.save!
  relative
end

def create_house_owner_relative(user, house)
  relative = Relative.new
  copy_user_values(relative, user)
  copy_plot_values(relative, house.plot)
  copy_house_values(relative, house)
  relative.family = HouseOwner.find_by_house_number(house.number)
  relative.save!
  relative
end

def create_tenant_relative(user, house)
  relative = Relative.new
  copy_user_values(relative, user)
  copy_plot_values(relative, house.plot)
  copy_house_values(relative, house)
  relative.family = Tenant.find_by_house_number(house.number)
  relative.save!
  relative
end

def copy_user_values(member, user)
  member.name = user.name
  member.email = user.email
  member.password = "aaaa"
  member.password_confirmation = "aaaa"
  member.user_id = user.id
end

def copy_plot_values(member, plot)
  member.phase = plot.phase
  member.plot_number = plot.number
  member.plot_area = plot.area
  member.plot_status = plot.status
  member.plot_id = plot.id
end

def copy_house_values(member, house)
  member.house_number = house.number
  member.house_area = house.area
  member.house_status = house.status
  member.rwh = house.rwh
  member.house_id = house.id
end

end
