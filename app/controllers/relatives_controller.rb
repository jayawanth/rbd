require_relative 'helpers/members_helper'

class RelativesController < ApplicationController
  helper_method :sort_column, :sort_direction
  include MembersHelper

  def new
    @relative = Relative.new
  end

  def show
    @relative = Relative.find(params[:id])
  end

  def create
    member_type, email, plot_number, house_number = params[:relative][:member_type], params[:relative][:email], params[:relative][:plot_number], params[:relative][:house_number]
    relative = get_relative(email)
    if relative
      flash[:alert] = "Relative::Create: Error - Relative with email: #{email} already exists, Family of #{relative.family.email}"
      @relative = Relative.new
      render 'new' and return
    end
    user = User.find_by_email(email)
    plot = Plot.find_by_number(plot_number) if !plot_number.blank?
    house = House.find_by_number(house_number) if !house_number.blank?
    if !user || (!plot && !house)
      flash[:alert] = "Relative::Create: Error - Specified email/plot/house is invalid - #{email}, Plot: #{plot_number}, House: #{house_number}"
      @relative = Relative.new
      render 'new' and return
    end

    if plot && house
      flash[:alert] = "Relative::Create: Error - Cannot specify both Plot & House  - #{email}, Plot: #{plot_number}, House: #{house_number}"
      @relative = Relative.new
      render 'new' and return
    end

    if member_type == 'PlotOwner' && !plot ||
       member_type != 'PlotOwner' && !house
      flash[:alert] = "Relative::Create: Error - Need Plot # for PlotOwner and House # for HouseOwner/Tenant"
      @relative = Relative.new
      render 'new' and return
      end

    case member_type
      when 'PlotOwner'
        @relative = create_plot_owner_relative(user, plot)
      when 'HouseOwner'
        @relative = create_house_owner_relative(user, house)
      when 'Tenant'
        @relative = create_tenant_relative(user, house)
    end
    if !@relative
      flash[:alert] = "Relative::Create: Error - : #{relative} \n: #{user}\n: #{plot}\n: #{house}"
      @relative = Relative.new
      render "new" and return
    end

    flash[:notice] = "Created Relative of #{member_type} #{@relative.email}"
    # session[:user_id] = @user.id   # Login the user too
    redirect_to relatives_path
  end

  def index
    @relatives = Relative.order(sort_column + " " + sort_direction)
  end

  def destroy
    @relative = Relative.find(params[:id])
    flash[:notice] = "Relative '#{@relative.email}' deleted."
    @relative.destroy
    redirect_to relatives_path
  end

  private
  def sort_column
    columns = Relative.column_names
    columns.include?(params[:sort]) ? params[:sort]: "email"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end

end
