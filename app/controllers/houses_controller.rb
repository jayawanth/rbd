class HousesController < ApplicationController
  helper_method :sort_column, :sort_direction

  def new
    @house = House.new
  end

  def create
    house = params[:house]
    plot_number = house[:plot_number]
    @plot = Plot.find_by_number(plot_number)
    if !@plot
      flash[:alert] = "House::Create - Error: Plot #: #{plot_number} could not be found"
      @house = House.new
      render 'new' and return
    end
    @house = House.new(house)
    @house.plot_id = @plot.id
    if !@house.save
      flash[:alert] = "House::Create: Error - House # #{house[:number]}."
      @house = House.new
      render 'new' and return
    end

    flash[:notice] = "Created House # #{@house.number}."
    redirect_to houses_url
  end

  def index
    @houses = House.order(sort_column + " " + sort_direction)
  end

  def show
    @house = House.find(params[:id])
  end

  def edit
    @house = House.find params[:id]
  end

  def update
    @house = House.find params[:id]
    @house.update_attributes!(params[:house])
    flash[:notice] = "House # #{@house.number} was successfully updated."
    redirect_to house_path(@house)
  end

  def destroy
    @house = House.find(params[:id])
    @house.destroy
    flash[:notice] = "House '#{@house.number}' deleted."
    redirect_to houses_path
  end

  private
  def sort_column
    columns = House.column_names
    columns.include?(params[:sort]) ? params[:sort]: "number"
  end

  def sort_direction
    %w[asc, desc].include?(params[:sort_dir]) ? params[:sort_dir]: "asc"
  end
end
