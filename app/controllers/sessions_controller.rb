class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url
    else
      flash.now.alert = "Email or password is incorrect"
      render 'new'
    end
  end

  def destroy
    email = current_user.email
    session[:user_id] = nil # reset_session is OK too
    flash[:notice] = "User #{email} - Logged out successfully"
    redirect_to root_url
  end
end
