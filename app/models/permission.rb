class Permission
  def initialize(user)
    allow :sessions, [:new, :create, :destroy]
    allow :pages, [:home, :location]
    if user
      allow [:users, :plots, :houses], [:show, :index]
      allow [:plot_owners, :house_owners, :tenants, :relatives, :owners, :members], [:show, :index]
      allow_all if user.admin?
    end
  end

  def allow?(controller, action)
    @allow_all || @allowed_actions[[controller.to_s, action.to_s]]
  end

  private

  def allow_all
    @allow_all = true
  end

  def allow(controllers, actions)
    @allowed_actions ||= {}
    Array(controllers).each do |controller|
      Array(actions).each do |action|
        @allowed_actions[[controller.to_s, action.to_s]] = true
      end
    end
  end
end