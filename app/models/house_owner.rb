class HouseOwner < Owner
  has_many :relatives, :as => :family, :dependent => :destroy
  belongs_to :house
end