class Plot < ActiveRecord::Base
  has_many :houses
  has_one :owner, :class_name => "PlotOwner"

  attr_accessible :phase, :number, :status, :area

  validates_presence_of   :phase, :number, :status, :area
  validates_format_of     :phase, :with => /^[1|2|3|4]$/
  validates_format_of     :number, :with => /^\d{1,3}(A|B|(\/(1|2|A|B|1A|1B)))?$/
  validates_uniqueness_of :number, :case_sensitive => false
  validates_format_of     :status, :with => /^(empty|construction|house)$/, :message => "status %{value}"
  validates_numericality_of :area, :only_integer => true, :greater_than => 1000,  :less_than => 12000, :message => "Invalid Area - Range is 1000-12000 sft"

  # Class methods
  # <UIText, internal representation>
  def Plot.phase_range
    [ ["Phase 1", 1],
      ["Phase 2", 2],
      ["Phase 3", 3],
      ["Phase 4", 4],
    ]
  end

  # <UIText, internal representation>
  def Plot.status_options
    [["Empty Site", "empty"],
     ["House", "house"],
     ["Construction", "construction"]
    ]
  end

  # Instance methods - for a specific plot, return ...
  def status_text
    Plot.status_options.each do |elem|
      return elem[0] if elem[1] == status
    end
  end

  def phase_text
    Plot.phase_text(phase)
  end

  def Plot.phase_text(phase)
    Plot.phase_range.each do |elem|
      return elem[0] if elem[1] == phase
    end
  end
end
