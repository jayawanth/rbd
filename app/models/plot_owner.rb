class PlotOwner < Owner
  has_many :relatives, :as => :family, :dependent => :destroy
  belongs_to :plot
end
