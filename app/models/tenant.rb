class Tenant < Member
  has_many :relatives, :as => :family, :dependent => :destroy
  belongs_to :house
end