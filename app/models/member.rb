class Member < ActiveRecord::Base
  # this validates presence of password and password_confirmation
  has_secure_password
  attr_accessible :type
  attr_accessible :user_id, :name, :email, :password_digest, :password, :password_confirmation
  validates_presence_of :name, :email
  validates_format_of :email, :with => /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  attr_accessible :plot_id, :phase, :plot_number, :plot_status, :plot_area
  attr_accessible :house_id, :house_number, :house_status, :rwh, :house_area

  # class Relative uses these to setup polymorphic relationship to family of PlotOwner/HouseOwner/Tenant
  attr_accessible :family_id, :family_type

  def plot
    @plot ||= Plot.find(plot_id)
  end

  def user
    @user ||= User.find(user_id)
  end

  def house
    @house ||= House.find(house_id)
  end
end
require_dependency "owner"
require_dependency "tenant"
require_dependency "relative"