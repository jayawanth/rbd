class House < ActiveRecord::Base
  belongs_to  :plot
  has_one :owner, :class_name => "HouseOwner"
  has_one :tenant

  attr_accessible :phase, :plot_number, :plot_id, :number, :status, :rwh, :area

  validates_presence_of   :phase, :plot_number, :plot_id, :number, :status, :rwh, :area
  validates_format_of     :phase,   :with => /^[1|2|3|4]$/
  validates_format_of     :plot_number,  :with => /^\d{1,3}(A|B|(\/(1|2|A|B|1A|1B)))?$/
  validates_format_of     :number,  :with => /^\d{1,3}(A|B|(\/(1|2|A|B|1A|1B)))?$/
  validates_uniqueness_of :number,  :case_sensitive => false
  validates_format_of     :status,  :with => /^(empty|owner_occupied|rented)$/, :message => "status %{value}"
  validates_numericality_of :area,  :only_integer => true, :greater_than => 500, :less_than => 12000, :message => "Invalid Area - Range is 500-10000 sft"

  # Class methods
  # <UIText, internal representation>
  def House.status_options
    [["Empty", "empty"],
     ["Owner Occupied", "owner_occupied"],
     ["Rented", "rented"],
    ]
  end

  # Instance methods - for a specific house, return ...
  def status_text
    House.status_options.each do |elem|
      return elem[0] if elem[1] == status
    end
  end

  def phase_text
    Plot.phase_text(phase)
  end
end
