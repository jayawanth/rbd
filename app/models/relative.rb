class Relative < Member
  belongs_to :family, :polymorphic => true
  attr_accessor :member_type
  attr_accessible :member_type
  def Relative.member_types
    [ ["Plot Owner", 'PlotOwner'],
      ["House Owner", 'HouseOwner'],
      ["Tenant", 'Tenant'],
    ]
  end
end