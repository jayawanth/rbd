class User < ActiveRecord::Base
  has_secure_password

  attr_accessible :name, :email, :password, :password_confirmation
  validates_presence_of :name, :email
  validates_uniqueness_of :email, :case_sensitive => false
  validates_format_of :email, :with => /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates_length_of :password, :minimum => 4, :maximum => 12

  # validates_length_of :first_name, :last_name, :minimum => 2, :maximum => 50
  def admin?
    self.name.in? %w[Jayawanth]
  end
end
