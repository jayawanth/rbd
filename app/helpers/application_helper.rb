module ApplicationHelper
  def present(object, klass=nil)
    klass ||= "#{object.class}Presenter".constantize
    presenter = klass.new(object, self) # self is the view from where it is called
    yield presenter if block_given?
    presenter
  end

  def sortable_link_to(column, title = nil)
    title ||= column.titleize
    css_class = (sort_column == column)?  "current #{sort_direction}": nil
    sort_dir = (sort_column == column && sort_direction == "asc") ? "desc": "asc"
    link_to title, {:sort => column, :sort_dir => sort_dir}, {:class => css_class}
  end

end
