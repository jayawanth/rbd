class TenantPresenter < MemberPresenter
  presents :tenant

  def name
    h.link_to "#{tenant.name}", h.tenant_path(tenant)
  end

  def email
    h.link_to "#{tenant.email}", h.tenant_path(tenant)
  end

end