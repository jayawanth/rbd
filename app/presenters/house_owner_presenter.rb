class HouseOwnerPresenter < OwnerPresenter
  presents :house_owner

  def name
    h.link_to "#{house_owner.name}", h.house_owner_path(house_owner)
  end

  def email
    h.link_to "#{house_owner.email}", h.house_owner_path(house_owner)
  end

end
