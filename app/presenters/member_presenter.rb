class MemberPresenter < BasePresenter
  presents :member

  def name
    h.link_to "#{member.name}", h.member_path(member)
  end

  def email
    h.link_to "#{member.email}", h.member_path(member)
  end

  def type
    member.type
  end

  def plot_number
    h.link_to "Plot #{member.plot_number}", h.plot_path(member.plot_id)
  end

  def plot_status
    member.plot.status_text
  end

  def house_number
    return "-" if member.house_number.blank?
    h.link_to "House #{member.house_number}", h.house_path(member.house_id)
  end

  def house_status
    return "-" if member.house_number.blank?
    member.house.status_text
  end

  def rwh
    # calculate roof area and harvested water per year
    (member.rwh == nil || member.rwh == 0) ? "-": h.image_tag("rwh.jpg")
  end

  def relative
    return '-'
  end
end