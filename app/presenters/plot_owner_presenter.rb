class PlotOwnerPresenter < OwnerPresenter
  presents :plot_owner

  def name
    h.link_to "#{plot_owner.name}", h.plot_owner_path(plot_owner)
  end

  def email
    h.link_to "#{plot_owner.email}", h.plot_owner_path(plot_owner)
  end

end