class HousePresenter < BasePresenter
  presents :house

  def phase
    house.phase_text
  end

  def status
    house.status_text
  end

  def rwh
    house.rwh == 0? '-': h.image_tag('rwh.jpg')
  end

  def owner_name
    return '-' if house.owner_name.blank?
    h.link_to "#{house.owner_name}", h.house_owner_path(house.owner)
  end

  def tenant_name
    return '-' if house.status != 'rented' || house.tenant_name.blank?
    h.link_to "#{house.tenant_name}", h.tenant_path(house.tenant)
  end
end