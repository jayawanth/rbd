class OwnerPresenter < MemberPresenter
  presents :owner

  def name
    h.link_to "#{owner.name}", h.owner_path(owner)
  end

  def email
    h.link_to "#{owner.email}", h.owner_path(owner)
  end

end