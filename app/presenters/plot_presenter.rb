class PlotPresenter < BasePresenter
  presents :plot

  def owner_name
    return "-" if !plot.owner
    h.link_to "#{plot.owner.name}", h.user_path(plot.owner)
  end

  def phase
    plot.phase_text
  end

  def status
    plot.status_text
  end
end
