class RelativePresenter < MemberPresenter
  presents :relative

  def relative
    "Relative of " + member.family.email + '(' + member.family.type + ')'
  end
end